﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Framerate
{
    public class Program
    {
        public static void Main(String[] args)
        {

        }

        /// <summary>
        /// Convert string param framerate into integer value 
        /// </summary>
        public int Convert(string framerate)
        {
            if (string.IsNullOrWhiteSpace(framerate))
                return 0;

            framerate = framerate.Trim();
            bool isFraction = IsFraction(framerate);

            float number;
            if (isFraction)
                number = ConvertFractionToFloat(framerate);
            
            else
            {
                if (!float.TryParse(framerate, out number))
                    return 0;
            }

            if (number < 0)
                return 0;

            return Round2Nearest(number);
        }

        /// <remarks>Assume that all numbers in fractions are int numbers</remarks>
        private float ConvertFractionToFloat(string fraction)
        {          
            var numbers = new List<int>();

            var sb = new StringBuilder();
            foreach (char iCh in fraction)
            {
                if (iCh.Equals('/'))
                {
                    string rawNumb = sb.ToString();
                    sb.Clear();

                    if (int.TryParse(rawNumb, out int numb))
                        numbers.Add(numb);
                    
                    else
                        return 0;

                    continue;
                }

                sb.Append(iCh);
            }

            if (sb.Length > 0 && int.TryParse(sb.ToString(), out int number))
                numbers.Add(number);

            float res = numbers[0];
            for (int i = 1; i < numbers.Count; i++)
            {
                // Cannot divide by 0
                if (numbers[i] == 0)
                    return 0; 

                res /= numbers[i];
            }

            return res;
        }

        private int Round2Nearest(float number)
        {
            return (int)Math.Round(number, 0);
        }

        public bool IsFraction(string value)
        {
            Match match = Regex.Match(value, @"\d+(\.\d+)?/\d+(\.\d+)?");
            return match.Success;
        }
    }
}
