﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framerate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framerate.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        private Program Prog { get { return new Program(); } }

        [TestMethod()]
        public void ConvertTest_24()
        {
            Assert.AreEqual(24, Prog.Convert("23.976"));
        }

        [TestMethod()]
        public void ConvertTest_60()
        {
            Assert.AreEqual(60, Prog.Convert("59.94"));
        }

        [TestMethod()]
        public void ConvertTest_30_29()
        {
            Assert.AreEqual(30, Prog.Convert("29.97"));
        }

        [TestMethod()]
        public void ConvertTest_30_0()
        {
            Assert.AreEqual(30, Prog.Convert("30.0"));
        }

        [TestMethod()]
        public void ConvertTest_30()
        {
            Assert.AreEqual(30, Prog.Convert("30"));
        }

        [TestMethod()]
        public void ConvertTest_30_Fraction()
        {
            Assert.AreEqual(30, Prog.Convert("30000/1001"));
        }

        [TestMethod()]
        public void ConvertTest_NegativeFraction()
        {
            Assert.AreEqual(0, Prog.Convert("-30/1"));
        }

        [TestMethod()]
        public void ConvertTest_Letters()
        {
            Assert.AreEqual(0, Prog.Convert("abc"));
        }

        [TestMethod()]
        public void ConvertTest_InvalidString()
        {
            Assert.AreEqual(0, Prog.Convert(string.Empty));
            Assert.AreEqual(0, Prog.Convert(null));
            Assert.AreEqual(0, Prog.Convert("    "));
            Assert.AreEqual(0, Prog.Convert("1232sadasd"));
            Assert.AreEqual(0, Prog.Convert("30/string here"));
        }

        #region Test Prog.IsFraction method
        [TestMethod()]
        public void IsFraction_True()
        {
            Assert.IsTrue(Prog.IsFraction("1233/43"));
        }
        [TestMethod()]
        public void IsFraction_False()
        {
            Assert.IsFalse(Prog.IsFraction("123343"));
        }

        [TestMethod()]
        public void IsFraction_True_SeveralDivisions()
        {
            Assert.IsTrue(Prog.IsFraction("-1233/43/2"));
        }
        #endregion
    }
}